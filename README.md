# Simple Pokemon Apps

Try React 18 + 1 + 2  + 3 + 4

## Features:

- PWA
- Persistence Item
- Catch Pokemon

## Tech Stack

- GraphQL
- Emotion js
- Redux
- webpack

## Job shortage:

- The Unit Tests haven't finished yet.

## The Reason:

 I need more time to finished all unit test on component because many components have to be handled

## Screenshot:
|                                                                                                                                         |                                                                                                                                          |
| --------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| ![image.png](./image.png) | ![image-1.png](./image-1.png) |
|![image-2.png](./image-2.png)|![image-3.png](./image-3.png)|
|![image-4.png](./image-4.png)|
