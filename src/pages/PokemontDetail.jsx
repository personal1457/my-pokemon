import React, { useEffect, useState } from "react";
import {
  Card,
  TextPill,
  TextBold,
  TextItem,
  PokeBall,
  Container,
  GuideText,
  AlertText,
  CenterText,
  Navigation,
  MoveSection,
  ShooterLock,
  PokemonName,
  PokemonImage,
  HeaderSection,
  CustomTextPill,
} from "../assets/styled/PokemonDetail.styled";
import { Section } from "../assets/styled/Card.styled";
import { useNavigate, useParams } from "react-router-dom";
import shoot from "../assets/images/lock-target.png";
import arrowLeft from "../assets/images/left.png";
import pokemonBall from "../assets/images/pokeball.png";
import { Modal } from "../components/Modal";
import { useSelector } from "react-redux";
import { useQuery } from "@apollo/client";
import { GET_POKEMON_DETAIL } from "../queries/getPokemonDetail.query";

export const PokemonDetail = () => {
  const marginOfScreen = 80;
  const navigate = useNavigate();
  const { name } = useParams();
  const { itemSelected } = useSelector((state) => state.selectedItem);
  const [objectXPosition, setObjectXPosition] = useState(0);
  const [moveDirection, setMoveDirection] = useState("right");
  const [isEnableCatchMode, setEnableCatchMode] = useState(false);
  const [isShowAlert, setShowAlert] = useState(false);
  const [isShowSaveModal, setShowSaveModal] = useState(false);
  const { loading, error, data } = useQuery(GET_POKEMON_DETAIL, {
    variables: { name },
  });

  useEffect(() => {
    let intervalMove = "";
    const frameUpdateInterval = 1;

    if (!loading && !error) {
      document.getElementById("pokemon").style.left = `${objectXPosition}px`;
    }

    if (!itemSelected) {
      navigate("/");
    }

    if (isEnableCatchMode) {
      intervalMove = setInterval(() => {
        handleObjectMovement();
      }, frameUpdateInterval);
    }

    return () => {
      clearTimeout(intervalMove);
    };
  });

  const startPokemonMovement = () => {
    setEnableCatchMode(true);

    if (isEnableCatchMode) {
      handleCatchValidation();
    }
  };

  const handleObjectMovement = () => {
    updateMoveDirection();

    if (moveDirection === "right") {
      setObjectXPosition(objectXPosition + 1);
    } else {
      setObjectXPosition(objectXPosition - 1);
    }
  };

  const handleShowAlert = () => {
    setShowAlert(true);
    const alertVisibilityTime = 800;

    setTimeout(() => {
      setShowAlert(false);
    }, alertVisibilityTime);
  };

  const updateMoveDirection = () => {
    if (objectXPosition === window.innerWidth - marginOfScreen) {
      setMoveDirection("left");
    } else if (objectXPosition === -marginOfScreen) {
      setMoveDirection("right");
    }
  };

  const handleCatchValidation = () => {
    const centerOfScreen = window.innerWidth / 2 - 50;
    const catchAreaRight = centerOfScreen + 10;
    const catchAreaLeft = centerOfScreen - 45;

    if (objectXPosition <= catchAreaRight && objectXPosition >= catchAreaLeft) {
      setShowSaveModal(true);
      setEnableCatchMode(false);
    } else {
      handleShowAlert();
    }
  };

  //Network Handling
  if (loading) {
    return <CenterText>Loading Data...</CenterText>;
  }

  if (error) {
    return <CenterText>Ops.. something wrong, please try again!</CenterText>;
  }

  return (
    <Container background="lightgreen">
      <Navigation
        src={arrowLeft}
        alt="arrow-left"
        onClick={() => navigate("/")}
      />
      <HeaderSection>
        <PokemonName>{name}</PokemonName>
        <Section>
          {data.pokemon.abilities.map((pokemon, i) => (
            <TextPill key={i}>{pokemon.ability.name}</TextPill>
          ))}
        </Section>
        <PokemonImage src={itemSelected} alt="image" id="pokemon" />
        <ShooterLock
          isCatchPokemon={isEnableCatchMode}
          src={shoot}
          alt="lock"
        />
        {isShowAlert && <AlertText>Oppss...</AlertText>}
      </HeaderSection>

      <Card>
        {!isEnableCatchMode && !isShowSaveModal && (
          <GuideText>Click For Catch Pokemon !</GuideText>
        )}
        <PokeBall
          isCatchPokemon={isEnableCatchMode}
          onClick={startPokemonMovement}
          src={pokemonBall}
          alt="lock"
        />

        <TextBold>About</TextBold>
        <TextItem>- {`Weight : ${data.pokemon.weight}`}</TextItem>
        <TextBold>Type</TextBold>
        <TextItem>- {data.pokemon.types[0].type.name}</TextItem>
        <TextBold>Moves</TextBold>
        <MoveSection>
          {data.pokemon.moves.map((item, i) => (
            <CustomTextPill key={i}>{item.move.name}</CustomTextPill>
          ))}
        </MoveSection>
      </Card>
      {isShowSaveModal && (
        <Modal
          onCloseModal={(value) => setShowSaveModal(value)}
          itemName={name}
          image={itemSelected}
          weight={data.pokemon.weight}
          type={data.pokemon.types[0].type.name}
        />
      )}
    </Container>
  );
};
