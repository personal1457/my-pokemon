import { rest } from "msw";
import { setupServer } from "msw/node";
import { Provider } from "react-redux";

import { render } from "@testing-library/react";
import { PokemonList } from "./PokemonList";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import configureStore from "redux-mock-store";

const renderComponent = (element) => {
  const initialState = { myPokemonLists: [] };
  const mockStore = configureStore();
  let store;
  store = mockStore(initialState);
  const client = new ApolloClient({
    uri: "https://graphql-pokeapi.graphcdn.app",
    cache: new InMemoryCache(),
  });

  return render(
    <ApolloProvider client={client}>
      <Provider store={store}>
        <BrowserRouter>
          <Routes>{element}</Routes>
        </BrowserRouter>
      </Provider>
    </ApolloProvider>
  );
};
const originalError = console.error;
beforeAll(() => {
  console.error = (...args) => {
    if (
      /Warning: ReactDOM.render is no longer supported in React 18./.test(
        args[0]
      )
    ) {
      return;
    }
    originalError.call(console, ...args);
  };
});

afterAll(() => {
  console.error = originalError;
});

describe("PokemonList.jsx", () => {
  it("should Show loading text when main page fetching data", () => {
    const { getByText } = renderComponent(
      <Route path="/" element={<PokemonList />} />
    );

    const titlePage = getByText("Loading Data");
    expect(titlePage).toBeInTheDocument();
  });
});
