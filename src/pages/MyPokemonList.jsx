/** @jsxImportSource @emotion/react */
import { PillowCard } from "../components/PillowCard";
import { MyListContainer, Title } from "../assets/styled/PillowCard.styled";
import close from "../assets/images/close.png";
import { Navigation } from "../assets/styled/PokemonDetail.styled";
import { useNavigate } from "react-router-dom";
import { css } from "@emotion/react";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { restoreListFromLocalStorage } from "../features/myListSlice";

export const MyPokemonList = () => {
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const { myPokemonLists } = useSelector((state) => state.myPokemonList);

  useEffect(() => {
    const localData = localStorage.getItem("my-pokemon-list");
    const listLocalBackUp = localData ? JSON.parse(localData) : "";

    if (listLocalBackUp?.length >= 1) {
      dispatch(
        restoreListFromLocalStorage({ persistenceList: listLocalBackUp })
      );
    }
  }, []);

  return (
    <MyListContainer>
      <Navigation
        css={css`
          left: 85%;
        `}
        src={close}
        alt="arrow-left"
        onClick={() => navigate("/")}
      />

      <Title>My Pokemon List</Title>
      {myPokemonLists?.length > 0 ? (
        myPokemonLists.map((item, i) => (
          <PillowCard
            name={item.name}
            key={i}
            image={item.image}
            weight={item.weight}
            type={item.type}
          />
        ))
      ) : (
        <h3
          css={css`
            text-align: center;
            color: red;
            margin-top: 80px;
          `}
        >
          List is Empty !
        </h3>
      )}
    </MyListContainer>
  );
};
