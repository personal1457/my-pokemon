import {
  H3,
  Section,
  Container,
  TopNavigation,
} from "../assets/styled/PokemonList.styled.js";
import { Card } from "../components/Card";
import { useQuery } from "@apollo/client";
import { GET_POKEMON_LIST_QUERY } from "../queries/getPokemonList.query.js";
import { useNavigate } from "react-router-dom";

export const PokemonList = () => {
  const navigate = useNavigate();
  const { loading, error, data } = useQuery(GET_POKEMON_LIST_QUERY);

  if (loading) {
    return <H3>Loading Data</H3>;
  }

  if (error) {
    return <H3>Ops.. something wrong, please try again!</H3>;
  }

  return (
    <Container>
      <H3>My Pokemon List</H3>
      <TopNavigation onClick={() => navigate("/my-list")}>
        My List
      </TopNavigation>
      <Section>
        {data.pokemons?.results?.map((item, i) => (
          <Card image={item.dreamworld} name={item.name} id={item.id} key={i} />
        ))}
      </Section>
    </Container>
  );
};
