import { BrowserRouter, Routes, Route } from "react-router-dom";
import { PokemonList } from "../pages/PokemonList";
import { PokemonDetail } from "../pages/PokemontDetail";
import { MyPokemonList } from "../pages/MyPokemonList";

export const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PokemonList />} />
        <Route path="/detail/:name" element={<PokemonDetail />} />
        <Route path="/my-list" element={<MyPokemonList />} />
        <Route path="*" element={<h1>Page Not found</h1>} />
      </Routes>
    </BrowserRouter>
  );
};
