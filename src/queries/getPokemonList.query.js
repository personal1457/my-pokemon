import { gql } from "@apollo/client";

export const GET_POKEMON_LIST_QUERY = gql`
  query pokemons {
    pokemons(limit: 26, offset: 1) {
      count
      next
      previous
      status
      message
      results {
        id
        dreamworld
        name
      }
    }
  }
`;
