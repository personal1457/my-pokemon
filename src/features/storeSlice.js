import { createSlice } from "@reduxjs/toolkit";

const storeSlice = createSlice({
  name: "mainStore",
  initialState: {
    itemSelected: "",
  },

  reducers: {
    itemReducer: (state, action) => {
      state.itemSelected = action.payload.itemSelected;
    },
  },
});

export const { itemReducer } = storeSlice.actions;
export default storeSlice.reducer;
