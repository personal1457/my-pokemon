import reducers from "./myListSlice";

it("should can read state value properly", () => {
  let state;
  state = reducers(
    {
      selectedItem: { itemSelected: "https://test-url.com" },
      myPokemonList: { myPokemonLists: [] },
    },
    {
      type: "mainStore/itemReducer",
    }
  );
  expect(state).toEqual({
    selectedItem: {
      itemSelected: "https://test-url.com",
    },
    myPokemonList: { myPokemonLists: [] },
  });
});
