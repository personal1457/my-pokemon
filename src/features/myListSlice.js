import { createSlice } from "@reduxjs/toolkit";

const myListSlice = createSlice({
  name: "MyList",

  initialState: {
    myPokemonLists: [],
  },

  reducers: {
    addItemToList: (state, action) => {
      const temporaryList = {
        name: action.payload.name,
        image: action.payload.image,
        weight: action.payload.weight,
        type: action.payload.type,
      };

      state.myPokemonLists.push(temporaryList);
      syncStorage(state.myPokemonLists);
    },

    removeItemFromList: (state, action) => {
      state.myPokemonLists = state.myPokemonLists.filter(
        (item) => item.name !== action.payload.name
      );
      syncStorage(state.myPokemonLists);
    },

    restoreListFromLocalStorage: (state, action) => {
      state.myPokemonLists = action.payload.persistenceList;
    },
  },
});

const syncStorage = (updatedData) => {
  localStorage.setItem("my-pokemon-list", JSON.stringify(updatedData));
};

export const {
  addItemToList,
  removeItemFromList,
  restoreListFromLocalStorage,
} = myListSlice.actions;
export default myListSlice.reducer;
