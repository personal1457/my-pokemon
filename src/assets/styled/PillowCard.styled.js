import styled from "@emotion/styled";
import { PokemonName } from "./PokemonDetail.styled";
import { MainImage } from "./Card.styled.js";

export const MyListContainer = styled.div`
  padding: 0 16px;
`;

export const Title = styled(PokemonName)`
  font-size: 24px;
`;

export const CardItem = styled.div`
  margin-bottom: 16px;
  display: flex;
  width: 100%;
  padding: 16px 0;
  border: solid 1px gray;
  border-radius: 12px;
  justify-content: space-evenly;
  box-shadow: 5px 3px 28px -13px rgba(0, 0, 0, 0.88);
  -webkit-box-shadow: 5px 3px 28px -13px rgba(0, 0, 0, 0.88);
  -moz-box-shadow: 5px 3px 28px -13px rgba(0, 0, 0, 0.88);
`;

export const ImageItem = styled(MainImage)`
  width: 76px;
  height: 78px;
  margin-left: 16px;
  left: 0;
`;

export const RightSection = styled.div`
  display: inline-block;
  padding-left: 8px;
`;

export const CardTitle = styled.h3`
  text-align: left;
  margin: 2px 0;
`;

export const CardSubtitle = styled.p`
  font-size: 12px;
  padding-right: 12px;
  margin: 6px 0;
`;

export const ActionButton = styled.button`
  background-color: gray;
  border: none;
  border-radius: 12px;
  padding: 6px 8px;
  color: white;
  font-weight: 600;
  margin-right: auto;
`;
