import styled from "@emotion/styled";
import { InfoText } from "./Card.styled";
import { pulseAnimation, bounce } from "./animation.styled";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  background-color: ${(props) => props.background};
  color: white;
`;

export const HeaderSection = styled.div`
  padding: 24px;
`;

export const TextPill = styled(InfoText)`
  margin-right: 8px;
  color: #282828;
`;

export const PokemonImage = styled.img`
  margin-top: 8px;
  width: 150px;
  position: relative;
  height: 150px;
  object-fit: fill;
`;

export const Card = styled.div`
  background-color: white;
  position: fixed;
  border-radius: 40px;
  width: calc(100% - 65px);
  height: 100%;
  padding: 32px;
  color: black;
`;

export const TextBold = styled.div`
  font-size: 16px;
  font-weight: bold;
`;

export const ShooterLock = styled.img`
  width: 80px;
  opacity: 0.1;
  z-index: 5;
  position: absolute;
  top: 12rem;
  right: 0;
  left: 0;
  margin: auto;
  display: ${(props) => (props.isCatchPokemon ? "block" : "none")};
`;

export const PokeBall = styled.img`
  width: 70px;
  z-index: 5;
  position: absolute;
  top: -40px;
  right: 0;
  left: 0;
  margin: auto;

  ${(props) => (props.isCatchPokemon ? pulseAnimation : "")}
`;

export const AlertText = styled.div`
  position: absolute;
  top: 8em;
  right: 50%;
  left: 40%;
  font-weight: 700;
  font-size: larger;
  color: red;
  z-index: 8;
  ${bounce}
`;

export const Navigation = styled.img`
  position: absolute;
  top: 10px;
  left: 10px;
  opacity: 0.4;
  width: 25px;
`;

export const PokemonName = styled.h1`
  color: black;
  text-transform: capitalize;
`;

export const GuideText = styled.p`
  font-size: 12px;
  text-align: center;
  font-weight: 600;
  margin: 2 0 4px 0;
  ${pulseAnimation}
`;

export const CustomTextPill = styled.div`
  margin: 10px 15px 0 0;
  padding: 4px 10px;
  width: fit-content;
  height: fit-content;
  background: rgba(42, 42, 42, 0.05);
  border-radius: 16px;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(7.7px);
  -webkit-backdrop-filter: blur(7.7px);
  border: 1px solid rgba(42, 42, 42, 0.46);
`;

export const TextItem = styled.p`
  font-style: italic;
  text-transform: capitalize;
  margin: 8px 0;
`;

export const MoveSection = styled.div`
  margin-top: 6px;
  display: flex;
  flex-wrap: wrap;
  align-content: flex-start;
  height: calc(100% - 526px);
  overflow-y: auto;
`;

export const CenterText = styled.h2`
  text-align: center;
  margin-top: 30%;
`;
