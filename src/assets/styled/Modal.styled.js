import styled from "@emotion/styled";
import { fadeInUp, pulseAnimation } from "./animation.styled";
import { PokemonImage } from "./PokemonDetail.styled";

export const ModalContainer = styled.div`
  bottom: 0;
  height: 84%;
  color: black;
  z-index: 100;
  padding: 18px;
  text-align: center;
  position: fixed;
  border-top-left-radius: 40px;
  border-top-right-radius: 40px;
  width: calc(100% - 35px);
  background-color: #d5d4d4;
  display: inline-block;
  ${fadeInUp}
`;

export const ImageContent = styled(PokemonImage)`
  width: 130px;
  height: 150px;
  animation-iteration-count: infinite;
  ${pulseAnimation}
`;

export const Title = styled.div`
  margin-top: 24px;
  font-size: 28px;
  font-weight: 600;
  text-transform: capitalize;
`;

export const TextInput = styled.input`
  margin-top: 28px;
  font-size: large;
  padding: 8px;
  border-radius: 18px;
  border: none;
  text-align: center;
  text-transform: capitalize;
`;

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  position: fixed;
  width: inherit;
  bottom: 0;
  padding: 12px 0;
`;

export const Button = styled.button`
  padding: 10px 14px;
  border-radius: 8px;
  font-size: large;
  width: 40%;
  border: none;
  margin-bottom: 20px;
`;

export const AlertDuplicateText = styled.p`
  color: red;
`;
