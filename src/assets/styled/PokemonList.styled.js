import styled from "@emotion/styled";

export const Section = styled.section`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;
export const H3 = styled.h3`
  margin-left: 18px;
`;

export const TopNavigation = styled.div`
  font-weight: 700;
  position: absolute;
  top: 2%;
  left: 75%;
  background-color: gray;
  color: white;
  padding: 2px 12px;
  border-radius: 10px;
  box-shadow: 0px -1px 15px -4px rgba(0, 0, 0, 0.88);
`;

export const Container = styled.div`
  display: inline-block;
`;
