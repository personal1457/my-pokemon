import styled from "@emotion/styled";

export const Container = styled.div`
  border: 1px solid gray;
  border-radius: 24px;
  width: 140px;
  padding: 12px;
  margin-bottom: 12px;
  position: relative;
  z-index: 1;
  background-color: ${(props) => props.background};
  color: white;
`;

export const H3 = styled.h3`
  max-width: 140px;
  overflow: hidden;
  text-align: center;
  font-weight: bold;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: black;
  margin: 0;
  text-transform: capitalize;
`;

export const Section = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const MainImage = styled.img`
  width: 110px;
  height: 100px;
  position: relative;
  left: 35%;
`;

export const Background = styled.img`
  width: 150px;
  opacity: 0.5;
  z-index: -1;
  position: absolute;
  top: 10%;
  left: 20%;
`;

export const InfoSection = styled.div`
  display: inline-block;
  top: 5em;
  position: absolute;
  color: gray;
`;

export const InfoText = styled.div`
  padding: 6px;
  font-size: 10px;
  font-weight: 800;
  margin-bottom: 0.5em;
  background: rgba(255, 255, 255, 0.29);
  border-radius: 16px;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(6.1px);
  -webkit-backdrop-filter: blur(6.1px);
  border: 1px solid rgba(255, 255, 255, 0.42);
`;
