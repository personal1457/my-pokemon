import { configureStore } from "@reduxjs/toolkit";
import itemReducer from "../features/storeSlice";
import myListReducer from "../features/myListSlice";

export const store = configureStore({
  reducer: {
    selectedItem: itemReducer,
    myPokemonList: myListReducer,
  },
});
