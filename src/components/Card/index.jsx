/** @jsxImportSource @emotion/react */
import {
  H3,
  Section,
  InfoText,
  MainImage,
  Container,
  Background,
  InfoSection,
} from "../../assets/styled/Card.styled";
import { useNavigate } from "react-router-dom";
import pokemonBall from "../../assets/images/pokemon_ball";
import { useDispatch } from "react-redux";
import { itemReducer } from "../../features/storeSlice";

export const Card = (props) => {
  const dispatch = useDispatch();
  let navigate = useNavigate();

  const handleClickCard = () => {
    dispatch(itemReducer({ itemSelected: props.image }));
    navigate(`/detail/${props.name}`);
  };

  const generateItemId = (id) => {
    const idLength = id.toString().length;
    return idLength === 1 ? `#00${id}` : idLength === 2 ? `#0${id}` : `#${id}`;
  };

  return (
    <Container background={props.background} onClick={handleClickCard}>
      <H3>{props.name}</H3>
      <Section>
        <InfoSection>
          <InfoText>{generateItemId(props.id)}</InfoText>
        </InfoSection>
        <MainImage src={props.image} alt="pokemon" />
        <Background src={pokemonBall} alt="" srcset="" />
      </Section>
    </Container>
  );
};
