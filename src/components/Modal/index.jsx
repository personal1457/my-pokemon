import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  Title,
  Button,
  Footer,
  TextInput,
  ImageContent,
  ModalContainer,
  AlertDuplicateText,
} from "../../assets/styled/Modal.styled";
import { addItemToList } from "../../features/myListSlice";

export const Modal = (props) => {
  const inputName = useRef(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { myPokemonLists } = useSelector((state) => state.myPokemonList);

  const [isDuplicate, setIsDuplicate] = useState(false);

  useEffect(() => {
    inputName.current.value = props.name;

    return () => {
      setIsDuplicate(false);
    };
  }, [props]);

  const handleClickSave = () => {
    if (checkDuplicateItem()) {
      setIsDuplicate(true);
      return;
    }

    try {
      dispatch(
        addItemToList({
          name: inputName.current.value,
          image: props.image,
          weight: props.weight,
          type: props.type,
        })
      );
    } catch (error) {
      console.error(error);
    } finally {
      navigate("/my-list");
    }
  };

  const checkDuplicateItem = () => {
    return myPokemonLists.some((item) => item.name === inputName.current.value);
  };

  useEffect(() => {
    inputName.current.value = props.itemName;
  });

  const handleClickCloseBtn = () => {
    props.onCloseModal(false);
  };

  return (
    <ModalContainer>
      <ImageContent src={props.image} alt="pokemon" />
      <TextInput placeholder="Custom Name" ref={inputName} />
      {isDuplicate && (
        <AlertDuplicateText>
          Opss.. Please use another name !
        </AlertDuplicateText>
      )}
      <Title>Weight: {props.weight}</Title>
      <Title>type: {props.type}</Title>
      <Footer>
        <Button onClick={handleClickCloseBtn}>Close</Button>
        <Button onClick={handleClickSave}>Save</Button>
      </Footer>
    </ModalContainer>
  );
};
