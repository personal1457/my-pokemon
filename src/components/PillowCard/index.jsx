import React from "react";
import {
  CardItem,
  ImageItem,
  CardTitle,
  ActionButton,
  RightSection,
  CardSubtitle,
} from "../../assets/styled/PillowCard.styled.js";
import { Section } from "../../assets/styled/Card.styled.js";
import { useDispatch } from "react-redux";
import { removeItemFromList } from "../../features/myListSlice.js";
import { useNavigate } from "react-router-dom";
import { itemReducer } from "../../features/storeSlice.js";

export const PillowCard = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const HandleClickDelete = () => {
    dispatch(removeItemFromList({ name: props.name }));
  };

  const handleClickOpen = () => {
    dispatch(itemReducer({ itemSelected: props.image }));
    navigate(`/detail/${props.name}`);
  };
  return (
    <CardItem>
      <ImageItem src={props.image} alt="pokemon" />
      <RightSection>
        <CardTitle>{props.name}</CardTitle>
        <Section>
          <CardSubtitle>
            <b>Weight:</b> <i>{props.weight}</i>
          </CardSubtitle>
          <CardSubtitle>
            <b>Type:</b> <i>{props.type}</i>
          </CardSubtitle>
        </Section>
        <Section>
          <ActionButton onClick={handleClickOpen}>Open</ActionButton>
          <ActionButton onClick={HandleClickDelete}>Delete</ActionButton>
        </Section>
      </RightSection>
    </CardItem>
  );
};
